ASM=nasm 
ASM_FLAGS=-f elf64 


%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

build: main.o lib.o dict.o
	ld -o program $?

run: build
	./program

clear:
	rm -rf program *.o

.PHONY: build run clear