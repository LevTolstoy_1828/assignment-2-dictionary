section .data
%define newline_char 0x10
%define EXIT 60
 
section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT      ; номер системного вызова close
    syscall         ; системный вызов
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                        ; зануляем rax
    .loop:		
        cmp byte [rdi + rax], 0         ; проверяем, является ли текущий символ нуль-терминированным
	je .end	                            ; переходим, если это так
	inc rax			                    ; инкрементируем счетчик длины строки
	jmp .loop		                    ; переходим в loop опять
    .end:
        ret			                    ; возвращает длину в rax

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax        ; зануляем rax
    mov rsi, rdi        ; записываем адрес назначения в rsi
    call string_length  ; высчитываем длину строки
    mov rdx, rax        ; записываем строку в регистр данных
    mov rdi, 1          ; в stdout
    mov rax, 1          ; идентификатор "write" в rax
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi            ; используем стек для передачи указателя на символ в rsi
    mov rsi, rsp        ; устанавливаем указатель на стек
    pop rdi             ; очищаем стек
    mov rdi, 1          ; в stdout
    mov rax, 1          ; идентификатор "write" в rax
    mov rdx, 1          ; длина char = 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rax, 1              ; идентификатор "write" в rax
    mov rdi, 1              ; в stdout
    mov rsi, newline_char   ; перевод строки ("10") в stdout
    mov rdx, 1              ; длина char = 1
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi            ; записываем число в rax
    mov rsi, rsp             ; сохраняем начальный указатель на стек
    mov r9, 10              ; записываем оснований сс в r9
    push 0                  ; загружаем в стек конец строки
    .loop:
        mov rdx, 0          ; зануляем регистр данных
        div r9              ; целочисленную часть помещаем в rax, остаток - в rdx
        add rdx, 48         ; переводим цифру в ASCII
        dec rsp             ; декрементируем указатель стека
        mov byte[rsp], dl   ; кладем цифру на стек
        cmp rax, 0          ; сравниваем с нулем
        ja .loop            ; если записано не все число, то переход в loop
        mov rdi, rsp        ; сохраняем указатель на стек
        push rsi             ; сохраняем изначальный указатель на стек
        call print_string   ; выводим число 
        pop rsi
        mov rsp, rsi         ; восстанавливаем стек до изначального состояния
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0          ; сравниваем число с нулем
    jge print_uint      ; если число положительное, то обрабатываем его, как беззнаковое
    mov r9, rdi         ; иначе сохраняем число в r9
    mov rdi, '-'        
    call print_char          ; выводим '-'
    mov rdi, r9         ; возвращаем число
    neg rdi             ; меняем знак
    call print_uint     ; выводим модуль числа
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    xor rcx, rcx
    .loop:
        mov r8b, byte [rdi + rcx]   ; берем символ из первой строки      
        mov r9b, byte [rsi + rcx]   ; берем символ из второй строки
        cmp r8b, r9b                ; сравниваем
        jne .false                  ; если они не равны, то переходим в false
        cmp r8b, 0                  ; если равны, то проверяем на конец строки
        je .true                    ; если конец строки, то переходим в true
        inc rcx                     ; увеличиваем счетчик 
        jmp .loop                  
    .false:
        mov rax, 0                  ; если не равны, то возвращаем 0
        ret
    .true:
        mov rax, 1                  ; если равны, то возвращаем 1
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdx, 1          ; длина строки в байтах
    xor rdi, rdi        ; stdin дескриптор 
    push rdi            ; выделяем в стеке память под считываемый символ
    mov rsi, rsp        ; устанавливаем адрес чтения
    syscall
    pop rax             ; помещаем символ в rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx                ; счетчик
    .loop:
        push rcx            
        push rdi                ; адрес начала буффера
        push rsi                ; длина буффера 
        call read_char          ; считываем символ
        pop rsi                 ; сохраняем переменные в стек
        pop rdi
        pop rcx
        cmp rax, 0x20           ; проверяем, является ли пробелом
        je .skip                ; если пробел, то переход в .skip
        cmp rax, 0x9            ; проверяем, является ли табуляцией
        je .skip                ; если табуляция, то переход в .skip
        cmp rax, 0xA            ; проверяем, является переносом строки
        je .skip                ; если перенос строки, то переход в .skip
        test rax, rax              ; проверяем, является ли концом строки
        je .end                 ; если конец строки, то переходим в .end
        mov [rdi + rcx], rax    ; если простой символ, то записываем в ячейку по индексу
        inc rcx                 ; увеличиваем счетчик
        cmp rcx, rsi            ; проверка на выход за границы буффера
        jge .limit              ; если выходит, то переход в .limit
        jmp .loop               ; иначе переход к следующему символу
    .skip:
        test rcx, rcx
        je .loop                ; если слово еще не считывается, то переходим к следующему символу
        jmp .end                ; если это конец слова, переходим в .end
    .limit:
        xor rax, rax            ; возвращаем 0
        ret
    .end:
        xor rax, rax        
        mov [rdi + rcx], rax    ; сохраняем 0 - конец строки
        mov rax, rdi            ; сохраняем адрес буффера
        mov rdx, rcx            ; сохраняем длину
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                    ; для числа
    xor rcx, rcx                    ; для длины числа
    .loop:
        xor r8, r8                  
        mov r8b, byte [rdi + rcx]   ; получаем символ
        cmp r8b, '0'                
        jb .end                     ; если меньше '0', то переход в .end
        cmp r8b, '9'
        ja .end                     ; если больше '9', то переход в .end
        inc rcx                     ; увеличиваем счетчик длины
        sub r8b, '0'                ; зануляем 
        imul rax, 10                ; умножаем rax на 10
        add rax, r8                 ; добавляем в rax новый символ
        jmp .loop                   
    .end:
        mov rdx, rcx                ; возвращаем в rdx длину числа
        ret     





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx            ; зануляем счетчик
    cmp byte[rdi], '-'      
    je .negative            ; если число отрицательное, то переход в .negative
    jmp parse_uint          ; если число положительное, то обрабатываем, как беззнаковое
    .negative:   
        inc rdi             ; смещаем строку на один символ, на '-'
        call parse_uint     ; парсим число
        neg rax             ; меняем знак, переводим в доп. код
        inc rdx             ; добавляем 1 в длину для знака
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax                
    xor r8, r8                      ; для длины строки 
    push rdi                        ; сохраняем регистры, которые могут быть изменены
    push rsi
    push rdx
    call string_length              ; получаем длину строки
    pop rdx 
    pop rsi
    pop rdi
    xor rcx, rcx                    ; зануляем счетчик для .loop
    mov r8, rax                     ; сохраняем длину
    cmp rax, rdx                    ; сравниваем размер буффера и длину строки 
    jbe .loop                       ; если вмещается, то переходим в .loop
    xor rax, rax                    ; если нет, возвращаем 0
    ret
    .loop:                  
        cmp rcx, r8                 ; проверяем на конец строки
        jg .end                     
        mov r9, [rdi + rcx]         ; помещаем символ в буффер 
        mov [rsi + rcx], r9
        inc rcx 
        jmp .loop
    .end:
        mov rax, r8                 ; возвращаем длину строки
        ret

print_error:
    mov rsi, 2
    push    rdi             
    push    rsi
    call    string_length   
    pop     rdi             
    pop     rsi             
    mov     rdx, rax        
    mov     rax, 1
    syscall
    ret



